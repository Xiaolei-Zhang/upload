package com.jieyin.upload.service;

import org.influxdb.dto.QueryResult;

import java.util.List;

public interface DataService {

    public <T> List<T> getResult(String query, Class<T> clazz);

    public List<QueryResult.Result> getResult(String query);
}
