package com.jieyin.upload.service.impl;

import com.jieyin.upload.influxdb.InfluxDBConnect;
import com.jieyin.upload.service.DataService;
import lombok.extern.slf4j.Slf4j;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Service
public class DataServiceImpl implements DataService {

    @Resource
    InfluxDBConnect influxDBConnect;

    @Override
    public <T> List<T> getResult(String query, Class<T> clazz) {
        log.info("query:{}", query);
        QueryResult queryResult = influxDBConnect.query(query);
        InfluxDBResultMapper resultMapper = new InfluxDBResultMapper();
        return resultMapper.toPOJO(queryResult, clazz);
    }

    @Override
    public List<QueryResult.Result> getResult(String query) {
        log.info("query:{}", query);
        QueryResult queryResult = influxDBConnect.query(query);
        return queryResult.getResults();
    }
}
