package com.jieyin.upload.influxdb;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author xucj
 * @create 2021-01-25
 * @Description:
 */

@Configuration
@Slf4j
public class InfluxDBConfiguration {

    @Resource
    private InfluxDBProperties influxDBProperties;

    @Bean
    public InfluxDBConnect getInfluxDBConnect() {
        InfluxDBConnect influxDB = new InfluxDBConnect(influxDBProperties.getUserName(), influxDBProperties.getPassword(),
                influxDBProperties.getUrl(), influxDBProperties.getDatabase());

        influxDB.influxDbBuild();

        log.info("init influxdb::[{}]", influxDBProperties);
        return influxDB;
    }
}
