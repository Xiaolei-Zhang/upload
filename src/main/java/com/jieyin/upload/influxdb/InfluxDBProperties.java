package com.jieyin.upload.influxdb;

import lombok.Data;
import org.hibernate.validator.constraints.URL;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotBlank;

/**
 * @author xucj
 * @create 2021-03-10
 * @Description:
 */

@Configuration
@ConfigurationProperties(prefix = "spring.influx")
@EnableConfigurationProperties(InfluxDBProperties.class)
@Data
public class InfluxDBProperties {


    @URL
    private String url;
    @NotBlank
    private String userName;
    @NotBlank
    private String password;
    @NotBlank
    private String database;

}
