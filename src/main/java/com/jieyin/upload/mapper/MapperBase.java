package com.jieyin.upload.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface MapperBase<T> extends BaseMapper<T> {
}
